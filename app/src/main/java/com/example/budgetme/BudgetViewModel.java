package com.example.budgetme;


import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

/**
 * ViewModel for Budget
 */
public class BudgetViewModel extends ViewModel {
    private static int currentPosition;
    private static int editPosition;
    public static int totalSum = 0;
    private static ArrayList<BudgetObject> BudgetArrayList;

    /**
     * Gets the ArrayList of BudgetObjects or creates a new ArrayList and returns it.
     *
     * @return BudgetArrayList
     */
    public static ArrayList<BudgetObject> getBudgetArrayList() {
        if (BudgetArrayList == null) {
            BudgetArrayList = new ArrayList<>();

        }
        return BudgetArrayList;
    }

    /**
     * Adds newBudget to the ArrayList of BudgetObjects
     *
     * @param newBudget
     */
    public static void addBudget(String newBudget) {
        BudgetArrayList.add(new BudgetObject(newBudget));

    }

    /**
     * Adds newBudget to the ArrayList of BudgetObjects
     *
     * @param newBudget
     */
    public static void addBudget(String newBudget, int newAmount) {
        BudgetArrayList.add(new BudgetObject(newBudget, newAmount));
        totalSum += newAmount;
    }

    /**
     * Changes the name of the Budget at position to changed Text.
     *
     * @param position    Budget position to be changed
     * @param changedText New Name of the Budget.
     */
    public static void changeBudget(int position, String changedText, int changedAmount) {
        totalSum -= BudgetArrayList.get(position).getBudgetAmount();
        BudgetArrayList.get(position).setBudgetName(changedText);
        BudgetArrayList.get(position).setBudgetAmount(changedAmount);
        totalSum += BudgetArrayList.get(position).getBudgetAmount();
    }

    /**
     * Removes a Budget at position
     *
     * @param position Budget position to be removed
     */
    public static void removeBudget(int position) {
        totalSum -= BudgetArrayList.get(position).getBudgetAmount();
        BudgetArrayList.remove(position);
    }

    /**
     * Returns the Size of the ArrayList.
     *
     * @return Size of BudgetArrayList
     */
    public static int getBudgetSize() {
        return BudgetArrayList.size();
    }

    /**
     * Gets the Record Budget at position or creates a new one if null.
     *
     * @param position
     * @return ArrayList<String>
     */
    public static ArrayList<RecordObject> getRecordArrayList(int position) {
        if (BudgetArrayList.get(position).getRecordList() == null) {

            BudgetArrayList.get(position).setRecordList(new ArrayList<RecordObject>());

        }
        return BudgetArrayList.get(position).getRecordList();
    }

    /**
     * Sets the current position for the RecordActivity
     *
     * @param position
     */
    public static void setCurrentPosition(int position) {
        currentPosition = position;
    }

    /**
     * Returns the CurrentPosition set for RecordActivity.
     *
     * @return Position
     */
    public static int getCurrentPosition() {
        return currentPosition;
    }

    /**
     * Sets the edit position for editing a Budget or Record.
     *
     * @param position
     */
    public static void setEditPosition(int position) {
        editPosition = position;
    }

    /**
     * Returns the position to be edited
     *
     * @return editPosition
     */
    public static int getEditPosition() {
        return editPosition;
    }

    public static String getSum(){
        return " Total: $" + totalSum;
    }

    public static int getTotalSum() { return totalSum; }

    public static ArrayList<BudgetObject> getArrayList() { return BudgetArrayList; }

    /**
     * Creates a Budget for testing (Not needed for Final Product Only for testing purposes)
     */
    public void createTestBudget() {

        if (BudgetArrayList == null) {
            BudgetArrayList = new ArrayList<BudgetObject>();
            BudgetArrayList.add(new BudgetObject("Main Budget", 500));
            ArrayList<RecordObject> testRecords = new ArrayList<RecordObject>();
            testRecords.add(new RecordObject("Taco", "1/2/10", 6));
            testRecords.add(new RecordObject("Groceries", "1/4/10", 109));
            testRecords.add(new RecordObject("Gas", "1/5/10", 56));
            testRecords.add(new RecordObject("More Taco", "1/6/10", 5));
            testRecords.add(new RecordObject("Even more Taco", "1/7/10", 4));

            BudgetArrayList.get(0).setRecordList(testRecords);

            BudgetArrayList.add(new BudgetObject("Gas", 400));
            BudgetArrayList.get(1).setRecordList(testRecords);


            BudgetArrayList.add(new BudgetObject("Restaurants", 400));


            BudgetArrayList.add(new BudgetObject("Vacation", 50));
            BudgetArrayList.add(new BudgetObject("Extra", 150));
            BudgetArrayList.add(new BudgetObject("Cash", 0));

            totalSum += BudgetObject.goalSum;

        }

    }


}