package com.example.budgetme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

/**
 * This Activity adds User given input to the list.
 */
public class AddRecord extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_record);
    }

    /**
     * OnClickListener for the add layout.
     *
     * @param view
     */
    public void RecordAddClicked(View view) {
        String NewRecordName = ((EditText) findViewById(R.id.NewRecordName)).getText().toString();
        String NewAmount = ((EditText) findViewById(R.id.NewRecordAmount)).getText().toString();
        String NewDate = ((EditText) findViewById(R.id.NewRecordDate)).getText().toString();

        if (NewRecordName.equals("")||NewAmount.equals("")||NewDate.equals("")) {

        } else {
            Intent intent = new Intent();
            intent.putExtra(Intent_Constants.EXTRA_NEWBUDGET, NewRecordName);
            intent.putExtra(Intent_Constants.EXTRA_NEWAMOUNT, NewAmount);
            intent.putExtra(Intent_Constants.EXTRA_NEWDATE, NewDate);
            setResult(Intent_Constants.RESULT_OK, intent);
            finish();
        }
    }
}