package com.example.budgetme;

import android.app.Activity;
import android.content.Intent;

import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

public class DrawerUtil {
    public static void getDrawer(final Activity activity, Toolbar toolbar) {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        PrimaryDrawerItem HomeActivity = new PrimaryDrawerItem().withIdentifier(1).withName("Home");
        PrimaryDrawerItem MapsActivity = new PrimaryDrawerItem().withIdentifier(2).withName("Google Maps");
        PrimaryDrawerItem Calendar = new PrimaryDrawerItem().withIdentifier(3).withName("Calendar");
        PrimaryDrawerItem Messenger = new PrimaryDrawerItem().withIdentifier(4).withName("Discussion Board");
        PrimaryDrawerItem Logout = new PrimaryDrawerItem().withIdentifier(5).withName("Logout");
        PrimaryDrawerItem PiChart = new PrimaryDrawerItem().withIdentifier(6).withName("PiChart");


        Drawer result = new DrawerBuilder()
                .withActivity(activity)
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withCloseOnClick(true)
                .withSelectedItem(-1)
                .addDrawerItems(
                        HomeActivity,
                        MapsActivity,
                        Calendar,
                        Messenger,
                        Logout,
                        PiChart
                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    if (drawerItem.getIdentifier() == 1) {
                        Intent intent = new Intent(activity, HomeActivity.class);
                        view.getContext().startActivity(intent);
                    }
                    if (drawerItem.getIdentifier() == 2) {
                        Intent intent = new Intent(activity, MapsActivity.class);
                        view.getContext().startActivity(intent);
                    }
                    if (drawerItem.getIdentifier() == 3) {
                        Intent intent = new Intent(activity, Calendar.class);
                        view.getContext().startActivity(intent);
                    }
                    if (drawerItem.getIdentifier() == 4) {
                        Intent intent = new Intent(activity, Messenger.class);
                        view.getContext().startActivity(intent);
                    }
                    if (drawerItem.getIdentifier() == 5) {
                        mAuth.signOut();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        view.getContext().startActivity(intent);
                    }
                    if (drawerItem.getIdentifier() == 6) {
                        Intent intent = new Intent(activity, PiChartActivity.class);
                        view.getContext().startActivity(intent);
                    }
                    return true;
                })
                .build();
    }
}
