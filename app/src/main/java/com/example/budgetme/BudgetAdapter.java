package com.example.budgetme;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * This Class is an BudgetAdapter for the Budget's RecyclerView.
 */
public class BudgetAdapter extends RecyclerView.Adapter<BudgetAdapter.BudgetViewHolder> {
    private ArrayList<BudgetObject> mBudgetArrayList;
    private OnBudgetClickListener mListener;

    /**
     * interface for BudgetAdapter's OnClickListeners
     */
    public interface OnBudgetClickListener {
        void onBudgetClick(int position);

        void onDeleteClick(int position);

        void onEditClick(int position);
    }

    /**
     * Sets the on Click Listener for the card, edit, and delete of each item in the Budget.
     *
     * @param Listener
     */
    public void setOnBudgetClickBudgetenter(OnBudgetClickListener Listener) {
        mListener = Listener;
    }

    /**
     * This class is the ViewHolder for the Budgets.
     */
    public static class BudgetViewHolder extends RecyclerView.ViewHolder {
        private TextView BudgetTextView;
        private TextView AmountTextView;
        private ImageView BudgetDeleteImage;
        private ImageView BudgetEditImage;

        /**
         * BudgetViewHolder with parameters.
         *
         * @param itemView Layout View to be passed to adapter
         * @param Listener Listener to be passed to adapter
         */
        public BudgetViewHolder(@NonNull View itemView, final OnBudgetClickListener Listener) {
            super(itemView);
            BudgetTextView = itemView.findViewById(R.id.budgetTextView);
            AmountTextView = itemView.findViewById(R.id.amountTextView);
            BudgetDeleteImage = itemView.findViewById(R.id.image_delete);
            BudgetEditImage = itemView.findViewById(R.id.image_edit);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            Listener.onBudgetClick(position);
                        }
                    }
                }
            });

            BudgetDeleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            Listener.onDeleteClick(position);
                        }
                    }
                }
            });

            BudgetEditImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            Listener.onEditClick(position);
                        }
                    }
                }
            });
        }
    }

    /**
     * BudgetAdapter constructor.
     *
     * @param BudgetArrayList Item Budget from ViewModel.
     */
    public BudgetAdapter(ArrayList<BudgetObject> BudgetArrayList) {
        mBudgetArrayList = BudgetArrayList;
    }

    /**
     * View Holder onCreate class
     *
     * @param parent
     * @param viewType
     * @return BudgetViewHolder
     */
    @NonNull
    @Override
    public BudgetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.budget_layout, parent, false);
        BudgetViewHolder lvh = new BudgetViewHolder(v, mListener);
        return lvh;
    }

    /**
     * BudgetAdapter's onBindViewHolder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull BudgetViewHolder holder, int position) {
        BudgetObject currentObject = mBudgetArrayList.get(position);

        holder.BudgetTextView.setText(currentObject.getBudgetName());
        holder.AmountTextView.setText("$"+currentObject.getBudgetAmount());
    }

    /**
     * Gets the Budget count of the Budget ArrayList.
     *
     * @return The number of items in the ArrayList
     */
    @Override
    public int getItemCount() {
        return mBudgetArrayList.size();
    }
}
