package com.example.budgetme;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthBudgetner;

    private RecyclerView BudgetRecyclerView;
    private BudgetAdapter BudgetAdapter;
    private RecyclerView.LayoutManager BudgetLayoutManager;
    private String NewListText;
    private BudgetViewModel BudgetModel;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.toolbar)
    public Toolbar toolBar;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthBudgetner);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        DrawerUtil.getDrawer(this, toolBar);

        mAuth = FirebaseAuth.getInstance();

        mAuthBudgetner = firebaseAuth -> {
            if (firebaseAuth.getCurrentUser()==null)
            {
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            }
        };

        //Budget Budget
        BudgetModel = new ViewModelProvider(this).get(BudgetViewModel.class);

        BudgetModel.createTestBudget();
        TextView t = findViewById(R.id.textTotalBudget);
        t.setText(BudgetViewModel.getSum());
        buildBudgetRecyclerView();

    }

    /**
     * OnClickListener for the FAB button that starts the AddList Activity.
     */
    public void BudgetFabClicked(View view) {
        Intent intent = new Intent();
        intent.setClass(HomeActivity.this, AddBudget.class);
        startActivityForResult(intent, Intent_Constants.NEWBUDGET_INTENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestcode, int resultCode, Intent data) {
        super.onActivityResult(requestcode, resultCode, data);

        if (requestcode == Intent_Constants.NEWBUDGET_INTENT_REQUEST_CODE) {

            String NewBudgetText = data.getStringExtra(Intent_Constants.EXTRA_NEWBUDGET);
            String NewAmount = data.getStringExtra(Intent_Constants.EXTRA_NEWAMOUNT);

            BudgetModel.addBudget(NewBudgetText, Integer.parseInt(NewAmount));
            BudgetAdapter.notifyItemInserted(BudgetModel.getBudgetSize());
        }
        if (requestcode == Intent_Constants.EDITBUDGET_INTENT_REQUEST_CODE) {
            String NewBudgetText = data.getStringExtra(Intent_Constants.EXTRA_NEWBUDGET);
            String NewAmount = data.getStringExtra(Intent_Constants.EXTRA_NEWAMOUNT);

            BudgetModel.changeBudget(BudgetModel.getEditPosition(), NewBudgetText, Integer.parseInt(NewAmount));
            BudgetAdapter.notifyItemChanged(BudgetModel.getEditPosition());
        }

        TextView t = findViewById(R.id.textTotalBudget);
        t.setText(BudgetViewModel.getSum());
    }

    private void buildBudgetRecyclerView() {
        BudgetRecyclerView = findViewById(R.id.budgetRecyclerView);
        BudgetRecyclerView.setHasFixedSize(true);
        BudgetLayoutManager = new LinearLayoutManager(this);
        BudgetAdapter = new BudgetAdapter(BudgetModel.getBudgetArrayList());

        BudgetRecyclerView.setLayoutManager(BudgetLayoutManager);
        BudgetRecyclerView.setAdapter(BudgetAdapter);

        BudgetAdapter.setOnBudgetClickBudgetenter(new BudgetAdapter.OnBudgetClickListener() {
            /**
             * OnClickListener for each Budget in the cardView in order to switch to the ItemActivity
             */
            @Override
            public void onBudgetClick(int position) {
                BudgetModel.setCurrentPosition(position);
                Intent intent = new Intent();
                intent.setClass(HomeActivity.this, RecordActivity.class);
                startActivityForResult(intent, Intent_Constants.ItemActivity_INTENT_REQUEST_CODE);
            }

            /**
             * OnClickListener that deletes a specified card view.
             */
            @Override
            public void onDeleteClick(int position) {
                BudgetModel.removeBudget(position);
                BudgetAdapter.notifyItemRemoved(position);
            }

            /**
             * OnClickListener that goes to the edit Activity using intents.
             */
            @Override
            public void onEditClick(int position) {
                BudgetModel.setEditPosition(position);
                Intent intent = new Intent();
                intent.setClass(HomeActivity.this, AddBudget.class);
                startActivityForResult(intent, Intent_Constants.EDITBUDGET_INTENT_REQUEST_CODE);
            }
        });
    }
}