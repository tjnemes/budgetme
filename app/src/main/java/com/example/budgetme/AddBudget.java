package com.example.budgetme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

/**
 * This Activity adds User given input to the list.
 */
public class AddBudget extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_budget);
    }

    /**
     * OnClickListener for the add layout.
     *
     * @param view
     */
    public void ListAddClicked(View view) {
        String NewBudgetName = ((EditText) findViewById(R.id.NewBudgetName)).getText().toString();
        String NewAmount = ((EditText) findViewById(R.id.NewBudgetAmount)).getText().toString();
        if (NewBudgetName.equals("")||NewAmount.equals((""))) {

        } else {
            Intent intent = new Intent();
            intent.putExtra(Intent_Constants.EXTRA_NEWBUDGET, NewBudgetName);
            intent.putExtra(Intent_Constants.EXTRA_NEWAMOUNT, NewAmount);
            setResult(Intent_Constants.RESULT_OK, intent);
            finish();
        }
    }
}