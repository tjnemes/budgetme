package com.example.budgetme;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity that Controls the display of Budget Records.
 */
public class RecordActivity extends AppCompatActivity {
    private RecyclerView RecordRecyclerView;
    private RecordAdapter RecordAdapter;
    private RecyclerView.LayoutManager RecordLayoutManager;
    private BudgetViewModel BudgetModel;

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.toolbar)
    public Toolbar toolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        DrawerUtil.getDrawer(this, toolBar);

        setTitle(BudgetModel.getBudgetArrayList().get(BudgetModel.getCurrentPosition()).getBudgetName());
        BudgetModel = new ViewModelProvider(this).get(BudgetViewModel.class);
        buildRecordRecyclerView(BudgetModel.getCurrentPosition());
    }

    /**
     * OnClickListener for the FAB button that starts the AddBudget Activity.
     */
    public void RecordFabClicked(View view) {
        Intent intent = new Intent();
        intent.setClass(RecordActivity.this, AddRecord.class);
        startActivityForResult(intent, Intent_Constants.ADDRECORD_INTENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestcode, int resultCode, Intent data) {
        super.onActivityResult(requestcode, resultCode, data);
        if (requestcode == Intent_Constants.ADDRECORD_INTENT_REQUEST_CODE) {
            String NewRecordName = data.getStringExtra(Intent_Constants.EXTRA_NEWBUDGET);
            String NewRecordAmount = data.getStringExtra(Intent_Constants.EXTRA_NEWAMOUNT);
            String NewRecordDate = data.getStringExtra(Intent_Constants.EXTRA_NEWDATE);


            BudgetModel.getBudgetArrayList().get(BudgetModel.getCurrentPosition()).getRecordList().add(new RecordObject(NewRecordName, NewRecordDate, Integer.parseInt(NewRecordAmount)));
            RecordAdapter.notifyItemInserted(BudgetModel.getBudgetArrayList().get(BudgetModel.getCurrentPosition()).getRecordList().size());

        }
        if (requestcode == Intent_Constants.EDITRECORD_INTENT_REQUEST_CODE) {
            String NewRecordName = data.getStringExtra(Intent_Constants.EXTRA_NEWBUDGET);
            String NewRecordAmount = data.getStringExtra(Intent_Constants.EXTRA_NEWAMOUNT);
            String NewRecordDate = data.getStringExtra(Intent_Constants.EXTRA_NEWDATE);

            BudgetModel.getBudgetArrayList().get(BudgetModel.getCurrentPosition()).changeRecord(BudgetModel.getEditPosition(), new RecordObject(NewRecordName, NewRecordDate, Integer.parseInt(NewRecordAmount)));
            RecordAdapter.notifyItemChanged(BudgetModel.getEditPosition());
        }
    }

    private void buildRecordRecyclerView(int position) {
        BudgetModel.setCurrentPosition(position);
        RecordRecyclerView = findViewById(R.id.RecordRecyclerView);
        RecordRecyclerView.setHasFixedSize(true);
        RecordLayoutManager = new LinearLayoutManager(this);
        RecordAdapter = new RecordAdapter(BudgetModel.getRecordArrayList(position));

        RecordRecyclerView.setLayoutManager(RecordLayoutManager);
        RecordRecyclerView.setAdapter(RecordAdapter);

        RecordAdapter.setOnRecordClickListener(new RecordAdapter.OnRecordClickListener() {

            /**
             * OnClickListener for each Record in the Budget used for identifying a Record as "In progress".
             */
            @Override
            public void onRecordClick(int RecordPosition) {
                BudgetModel.setEditPosition(RecordPosition);
                Intent intent = new Intent();
                intent.setClass(RecordActivity.this, AddRecord.class);
                startActivityForResult(intent, Intent_Constants.EDITRECORD_INTENT_REQUEST_CODE);
            }

            /**
             * OnClickListener that deletes a specified card view.
             */
            @Override
            public void onDeleteClick(int RecordPosition) {
                BudgetModel.getRecordArrayList(BudgetModel.getCurrentPosition()).remove(RecordPosition);
                RecordAdapter.notifyItemRemoved(RecordPosition);
            }

        });
    }
}