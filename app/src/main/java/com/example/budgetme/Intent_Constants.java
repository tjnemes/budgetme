package com.example.budgetme;

/**
 * Intent Constants for intent request and results
 */
public class Intent_Constants {
    /**
     * Creating a new Budget Request
     */
    public final static int NEWBUDGET_INTENT_REQUEST_CODE = 1;
    /**
     * Creating a new Budget Result
     */
    public final static int RESULT_OK = 1;
    /**
     * Result message.
     */
    public final static String EXTRA_NEWBUDGET = "NEWBUDGET";
    /**
     * Result message.
     */
    public final static String EXTRA_NEWAMOUNT = "NEWAMOUNT";
    /**
     * Result message.
     */
    public final static String EXTRA_NEWDATE = "NEWDATE";
    /**
     * Creating a new edit Request
     */
    public final static int EDITBUDGET_INTENT_REQUEST_CODE = 2;
    /**
     * Creating a new edit Request
     */
    public final static int ADDRECORD_INTENT_REQUEST_CODE = 3;
    /**
     * Creating a new edit Request
     */
    public final static int EDITRECORD_INTENT_REQUEST_CODE = 4;

    /**
     * Clicking a Item
     */
    public final static int ItemActivity_INTENT_REQUEST_CODE = 5;

}
