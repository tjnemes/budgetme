package com.example.budgetme;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import org.eazegraph.lib.charts.PieChart;
import org.eazegraph.lib.models.PieModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PiChartActivity extends AppCompatActivity {

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.toolbar)
    public Toolbar toolBar;

     // Create the objects of TextView and PieChart class
        TextView LEGBudget1, LEGBudget2, LEGBudget3, LEGBudget4, LEGBudgetOther,
                 LABBudget1, LABBudget2, LABBudget3, LABBudget4, LABBudgetOther,
                 TVBudget1, TVBudget2, TVBudget3, TVBudget4, TVBudgetOther;

        PieChart pieChart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pi_chart);

        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        DrawerUtil.getDrawer(this, toolBar);

            // Link those objects with their respective id's that
            // we have given in .XML file
            LEGBudget1 = findViewById(R.id.legendBudget1);
            LEGBudget2 = findViewById(R.id.legendBudget2);
            LEGBudget3 = findViewById(R.id.legendBudget3);
            LEGBudget4 = findViewById(R.id.legendBudget4);
            LEGBudgetOther = findViewById(R.id.legendBudgetOther);

            LABBudget1 = findViewById(R.id.labelBudget1);
            LABBudget2 = findViewById(R.id.labelBudget2);
            LABBudget3 = findViewById(R.id.labelBudget3);
            LABBudget4 = findViewById(R.id.labelBudget4);
            LABBudgetOther = findViewById(R.id.labelBudgetOther);

            TVBudget1 = findViewById(R.id.tvBudget1);
            TVBudget2 = findViewById(R.id.tvBudget2);
            TVBudget3 = findViewById(R.id.tvBudget3);
            TVBudget4 = findViewById(R.id.tvBudget4);
            TVBudgetOther = findViewById(R.id.tvBudgetOther);

            pieChart = findViewById(R.id.piechart);

            // Creating a method setData()
            // to set the text in text view and pie chart
            setData();
    }

        public void setData()
        {
            ArrayList<BudgetObject> currentBudgets = BudgetViewModel.getArrayList();
            int numOfBudgets = 4;

            // Set the percentage of budget used
            double totalSum = (double)BudgetViewModel.getTotalSum();
            int size = currentBudgets.size();
            double amount[] = new double[size];//(double)currentBudgets.get(0).getBudgetAmount();
            double percentage[] = new double[size];// = (1-(amount/totalSum)*100);
            String orderedNames[] = new String[size];
            Double tempAmount = 0.0;
            double otherAmount = 0.0;
            double otherPercent = 0.0;

            for(int i = 0; i < size; i++) {
                amount[i] = (double) currentBudgets.get(i).getBudgetAmount();
                orderedNames[i] = currentBudgets.get(i).getBudgetName();
            }

            for(int m = 0; m < size; m++){
                for(int n = 0; n < size; n++){
                    if(amount[m] > amount[n] && (m != n)){
                        double dTemp = amount[m];
                        String sTemp = orderedNames[m];
                        amount[m] = amount[n];
                        orderedNames[m] = orderedNames[n];
                        amount[n] = dTemp;
                        orderedNames[n] = sTemp;
                    }
                }
            }

            for(int i = 0; i < size; i++) {
                if (i < numOfBudgets) {
                    percentage[i] = (amount[i] / totalSum) * 100;
                    tempAmount += amount[i];
                }
                else
                    otherPercent += (amount[i] / totalSum) * 100;
            }

            otherAmount = totalSum - tempAmount;


            LEGBudget1.setText(orderedNames[0] + ": $" + Integer.toString((int)amount[0]));
            LEGBudget2.setText(orderedNames[1] + ": $" + Integer.toString((int)amount[1]));
            LEGBudget3.setText(orderedNames[2] + ": $" + Integer.toString((int)amount[2]));
            LEGBudget4.setText(orderedNames[3] + ": $" + Integer.toString((int)amount[3]));
            LEGBudgetOther.setText("Other: $" + Integer.toString((int)otherAmount));

            LABBudget1.setText(orderedNames[0]);
            LABBudget2.setText(orderedNames[1]);
            LABBudget3.setText(orderedNames[2]);
            LABBudget4.setText(orderedNames[3]);

            TVBudget1.setText(Integer.toString((int)Math.ceil(percentage[0])));
            TVBudget2.setText(Integer.toString((int)Math.ceil(percentage[1])));
            TVBudget3.setText(Integer.toString((int)Math.ceil(percentage[2])));
            TVBudget4.setText(Integer.toString((int)Math.ceil(percentage[3])));
            TVBudgetOther.setText(Integer.toString((int)Math.ceil(otherPercent)));
 /*           TVBudget1.setText(Integer.toString(((int)((1-(((float)currentBudgets.get(0).getBudgetAmount()) / ((float)BudgetViewModel.getTotalSum())))*100))));
            TVBudget2.setText(Float.toString((float)currentBudgets.get(0).getBudgetAmount()));
            TVBudget3.setText(Integer.toString(BudgetViewModel.getTotalSum()));
 */

            // Set the data and color to the pie chart ***WILL CRASH WITH LESS THAN 4 BUDGETS***
            pieChart.addPieSlice(
                    new PieModel(
                            //currentBudgets.get(0).getBudgetName(),
                            orderedNames[0],
                            Integer.parseInt(TVBudget1.getText().toString()),
                            Color.parseColor("#FFA726")));
            pieChart.addPieSlice(
                    new PieModel(
                            orderedNames[1],
                            Integer.parseInt(TVBudget2.getText().toString()),
                            Color.parseColor("#66BB6A")));
            pieChart.addPieSlice(
                    new PieModel(
                            orderedNames[2],
                            Integer.parseInt(TVBudget3.getText().toString()),
                            Color.parseColor("#EF5350")));
            pieChart.addPieSlice(
                    new PieModel(
                            orderedNames[3],
                            Integer.parseInt(TVBudget4.getText().toString()),
                            Color.parseColor("#29B6F6")));
            pieChart.addPieSlice(
                    new PieModel(
                            Double.toString(otherPercent),
                            Integer.parseInt(TVBudgetOther.getText().toString()),
                            Color.parseColor("#FF6200EE")));

            // To animate the pie chart
            pieChart.startAnimation();
        }
}



