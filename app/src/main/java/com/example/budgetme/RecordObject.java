
package com.example.budgetme;

/**
 * This class is for creating and interacting with RecordObjects
 */
public class RecordObject {

    private String RecordName;
    private int RecordAmount;
    private String RecordDate;

    /**
     * Creates a RecordObject with a RecordName given by the param.
     *
     * @param name
     */
    public RecordObject(String name, String date, int amount) {
        RecordName = name;
        RecordDate = date;
        RecordAmount = amount;
    }

    /**
     * Sets the RecordObjects name.
     *
     * @param name New name of the RecordObject.
     */
    public void setRecordName(String name) {
        RecordName = name;
    }

    /**
     * Sets the RecordObjects Date
     *
     * @param date New Date for RecordObject.
     */
    public void setRecordDate(String date) {
        RecordDate = date;
    }

    /**
     * Sets the RecordObjects amount
     *
     * @param amount New amount for RecordObject.
     */
    public void setRecordAmount(int amount) {
        RecordAmount = amount;
    }

    /**
     * Gets the current Record's Name
     *
     * @return RecordName
     */
    public String getRecordName() {
        return RecordName;
    }

    /**
     * Gets the current Record's Date
     *
     * @return RecordDate
     */
    public String getRecordDate() {
        return RecordDate;
    }

    /**
     * Gets the current Record's Amount
     *
     * @return RecordAmount
     */
    public int getRecordAmount() {
        return RecordAmount;
    }

}
