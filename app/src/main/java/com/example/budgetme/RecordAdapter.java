package com.example.budgetme;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

/**
 * This Class is an RecordAdapter for the Record RecyclerView.
 */
public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.RecordViewHolder> {
    private ArrayList<RecordObject> mRecordArrayList;
    private OnRecordClickListener mListener;

    /**
     * interface for RecordAdapter's OnClickListeners
     */
    public interface OnRecordClickListener {
        void onRecordClick(int position);

        void onDeleteClick(int position);

    }

    /**
     * Sets the on Click Listener for the card, edit, and delete of each Record in the list.
     *
     * @param listener
     */
    public void setOnRecordClickListener(OnRecordClickListener listener) {
        mListener = listener;
    }

    /**
     * This class is the ViewHolder for the Records.
     */
    public static class RecordViewHolder extends RecyclerView.ViewHolder {
        private TextView RecordName;
        private TextView RecordAmount;

        private ImageView RecordDeleteImage;

        /**
         * RecordViewHolder with parameters.
         *
         * @param RecordView Layout View to be passed to adapter
         * @param listener Listener to be passed to adapter
         */
        public RecordViewHolder(@NonNull View RecordView, final OnRecordClickListener listener) {
            super(RecordView);
            RecordName = RecordView.findViewById(R.id.recordName);
            RecordAmount = RecordView.findViewById(R.id.recordAmount);

            RecordDeleteImage = RecordView.findViewById(R.id.image_delete);

            RecordView.setOnClickListener(new View.OnClickListener() {
                /**
                 * onClick for the Card.
                 * @param view
                 */
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onRecordClick(position);
                        }
                    }
                }
            });

            RecordDeleteImage.setOnClickListener(new View.OnClickListener() {
                /**
                 * onClick for the Delete Image.
                 * @param view
                 */
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });

        }
    }

    /**
     * RecordAdapter constructor.
     *
     * @param RecordArrayList Record List from ViewModel.
     */
    public RecordAdapter(ArrayList<RecordObject> RecordArrayList) {
        mRecordArrayList = RecordArrayList;
    }

    /**
     * View Holder onCreate class
     *
     * @param parent
     * @param viewType
     * @return RecordViewHolder
     */
    @NonNull
    @Override
    public RecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.record_layout, parent, false);
        RecordViewHolder lvh = new RecordViewHolder(v, mListener);
        return lvh;
    }

    /**
     * RecordAdapter's onBindViewHolder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull RecordViewHolder holder, int position) {

        holder.RecordName.setText(mRecordArrayList.get(position).getRecordName());
        holder.RecordAmount.setText("$" + mRecordArrayList.get(position).getRecordAmount());

    }

    /**
     * Gets the Record count of the Record ArrayList.
     *
     * @return The number of Records in the ArrayList
     */
    @Override
    public int getItemCount() {
        return mRecordArrayList.size();
    }

}
