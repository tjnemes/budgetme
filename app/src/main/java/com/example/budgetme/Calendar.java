package com.example.budgetme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Calendar extends AppCompatActivity {

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.toolbar)
    public Toolbar toolBar;

    CompactCalendarView compactCalendar;
    private final SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM- yyyy", Locale.getDefault());

    private String event;
    private String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        DrawerUtil.getDrawer(this, toolBar);

        Button eventButton = findViewById(R.id.eventButton);
        EditText eventText = findViewById(R.id.eventText);
        EditText dateText = findViewById(R.id.dateText);

        final ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(null);

        compactCalendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendar.setUseThreeLetterAbbreviation(true);

        actionBar.setTitle(dateFormatMonth.format(compactCalendar.getFirstDayOfCurrentMonth()));

        eventButton.setOnClickListener(v -> {
            if (eventText.getText() != null) {
                event = eventText.getText().toString();
                date = dateText.getText().toString();
                date += " 08:00:00.000 UTC";

                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy HH:mm:ss.SSS zzz");
                Date date2 = null;
                try {
                    date2 = df.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                assert date2 != null;
                long epoch = date2.getTime();

                Event ev1 = new Event(Color.GREEN, epoch, event);
                compactCalendar.addEvent(ev1);
            }
        });

        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                Context context = getApplicationContext();
                List<Event> allEvents = compactCalendar.getEvents(dateClicked);
                String events = compactCalendar.getEvents(dateClicked).toString();
                StringBuilder events2 = new StringBuilder();

                if(allEvents != null) {
                    if(!allEvents.isEmpty()) {
                        String delims = getString(R.string.delims);
                        final String[] tokens = events.split(delims);
                        int i = 0;
                        int j = 0;
                        while(i < tokens.length) {
                            events2.append(tokens[j + 4]);
                            events2.append("\n");
                            i = i + 6;
                            j = j + 5;
                        }
                        Toast.makeText(context, events2.toString(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Nothing planned that day", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                actionBar.setTitle(dateFormatMonth.format(firstDayOfNewMonth));
            }
        });
    }
}