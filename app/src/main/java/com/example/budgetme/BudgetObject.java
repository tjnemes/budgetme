package com.example.budgetme;

import java.util.ArrayList;

/**
 * This class is for creating and interacting with BudgetObjects
 */
public class BudgetObject {
    private String BudgetName;
    private int BudgetAmount;
    public static int goalSum = 0;
    private int SumOfRecords = 0;
    private ArrayList<RecordObject> RecordList;


    /**
     * Creates a BudgetObject with a BudgetName given by the param.
     *
     * @param name
     */
    public BudgetObject(String name) {
        BudgetName = name;
        BudgetAmount = 0;
        RecordList = null;
    }

    /**
     * Creates a BudgetObject with a BudgetName given by the param.
     *
     * @param name
     * @param amount
     */
    public BudgetObject(String name, int amount) {
        BudgetName = name;
        BudgetAmount = amount;
        RecordList = null;
        goalSum += amount;
    }

    /**
     * Sets the BudgetObjects name.
     *
     * @param name New name of the BudgetObject.
     */
    public void setBudgetName(String name) {
        BudgetName = name;
    }

    /**
     * Sets the BudgetObjects Amount.
     *
     * @param amount New name of the BudgetObject.
     */
    public void setBudgetAmount(int amount) {

        goalSum -= amount;
        BudgetAmount = amount;
        goalSum += amount;

    }

    /**
     * Sets the Record Budget for the BudgetObject
     *
     * @param Records ArrayList of Strings
     */
    public void setRecordList(ArrayList<RecordObject> Records) {
        RecordList = Records;
        SumOfRecords = 0;

        for (RecordObject o: Records){
            SumOfRecords += o.getRecordAmount();
        }
    }

    /**
     * Gets the current Budget's Name
     *
     * @return BudgetName
     */
    public String getBudgetName() {
        return BudgetName;
    }

    /**
     * Gets the current Budget's Amount
     *
     * @return BudgetAmount
     */
    public int getBudgetAmount() {
        return BudgetAmount;
    }

    /**
     * Changes a String inside the BudgetObjects ArrayList<String>
     *
     * @param position    Record to be changed
     * @param changedRecord New Record
     */
    public void changeRecord(int position, RecordObject changedRecord) {
        SumOfRecords -= RecordList.get(position).getRecordAmount();
        RecordList.set(position, changedRecord);
        SumOfRecords += RecordList.get(position).getRecordAmount();
    }

    /**
     * Gets the ArrayList of Records
     *
     * @return RecordList
     */
    public ArrayList<RecordObject> getRecordList() {
        return RecordList;
    }
}
